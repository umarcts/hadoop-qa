# Flux Hadoop QA

This repository contains a collection of sample jobs for various software that
make up a Hadoop cluster. These jobs currently are used to test a cluster
running Cloudera's latest (or nearly latest) version of CDH. In particular,
there are jobs for:

* Hadoop Java API
* Hadoop Streaming
* PySpark
* Pig
* Hive
* Yelp's mrjob

Running the `qa` script will upload data to HDFS and run all of these jobs. Any
jobs that fail will be printed at the end of the run.

Each individual QA job can be run on its own by running `./qa <job_name`. A
queue may be specified with `--queue <queue_name>`. Available jobs:

* `hadoop_java` - Job written in Java that uses the Hadoop MapReduce API
* `hadoop_streaming` - Job written in Python for Hadoop Streaming
* `hive` - Job written in Hive's query language
* `mrjob` - Job written with [Yelp's mrjob](https://github.com/Yelp/mrjob)
* `pig` - Job written in Pig Latin
* `pyspark` - Job written in Python using Spark's python bindings
